package config

import "golang.org/x/net/context"

import "google.golang.org/appengine/datastore"
import "google.golang.org/appengine/memcache"

type Config struct {
	// Legistar site to scrape.
	// Please include trailing slash.
	LegistarBaseURL string
	// Olson time for legistar site
	TimeLocationName string

	// do we send updates when we see changes?
	UpdatesEnable bool

	// Twitter settings

	// do we send tweets?
	TwitterEnable bool
	// API Key
	TwitterKey string
	// API Secret
	TwitterSecret string
	// Access token
	TwitterToken string
	// Access token secret
	TwitterTokenSecret string
}

var cachedConfig *Config

// Set global config returned by GetConfig
func SetConfig(conf *Config) {
	localConf := *conf
	cachedConfig = &localConf
}

// Get configuration for app
func GetConfig(c context.Context) (*Config, error) {
	var localConf Config

	if cachedConfig != nil {
		localConf = *cachedConfig
		return &localConf, nil
	}

	conf, err := initConfig(c)
	if err == nil {
		SetConfig(conf)
		localConf = *conf
		return &localConf, nil
	}
	return nil, err
}

// Update configuration state (from admin utility or the like)
func UpdateConfig(c context.Context, update func(*Config) (*Config, error)) error {
	var newConf *Config

	err := datastore.RunInTransaction(c, func(tc context.Context) error {
		var txErr error
		newConf, txErr = updateConfigInTx(tc, update)
		return txErr
	}, nil)

	if err != nil {
		return err
	}

	if newConf == nil {
		return nil // ?!
	}

	updateMemcache(c, newConf)
	SetConfig(newConf)
	return nil
}

// Helper function to run the passed-in update function within a
// datastore transaction. The caller is assumed to have set things
// up appropriately.
func updateConfigInTx(c context.Context, update func(*Config) (*Config, error)) (*Config, error) {

	// TODO - round-trip fields we don't understand?

	oldConf, err := getFromDatastore(c)
	if err != nil {
		return nil, err
	}

	if oldConf == nil {
		oldConf = new(Config)
	}

	newConf, err := update(oldConf)
	if err != nil {
		return nil, err
	}

	err = updateDatastore(c, newConf)
	if err != nil {
		return nil, err
	}
	return newConf, nil
}

func initConfig(c context.Context) (*Config, error) {
	var conf *Config
	var err error
	var item *memcache.Item

	// Try memcache
	item, err = memcache.Gob.Get(c, configMemcacheKey(), conf)
	if err == nil {
		conf = item.Object.(*Config)
		return conf, nil
	}

	// Try datastore
	if item == nil {
		conf, err = getFromDatastore(c)
		if err != nil {
			return nil, err
		}
	}

	// If we still don't have a config, make a fresh one
	// and store it in memcache (not datastore, unless
	// we do it in a transaction).
	if conf == nil {
		conf = new(Config)
		updateMemcache(c, conf)
	}

	// Whatever we did, put it in memcache
	updateMemcache(c, conf)

	return conf, nil
}

// Write configuration to memcache
func updateMemcache(c context.Context, conf *Config) error {
	item := new(memcache.Item)
	item.Key = configMemcacheKey()
	item.Object = conf
	return memcache.Gob.Set(c, item)
}

func updateDatastore(c context.Context, conf *Config) error {
	_, err := datastore.Put(c, configKey(c), conf)
	return err
}

func getFromDatastore(c context.Context) (*Config, error) {
	var localConf Config
	var ok bool

	err := datastore.Get(c, configKey(c), &localConf)

	if err == nil {
		return &localConf, nil
	}

	if err == datastore.ErrNoSuchEntity {
		return nil, nil
	}

	// We ignore field-mismatch errors to make
	// data-migrations easy.
	if _, ok = err.(*datastore.ErrFieldMismatch); ok {
		return &localConf, nil
	}

	return nil, err
}

func configKey(c context.Context) *datastore.Key {
	return datastore.NewKey(c, "config", "appconfig", 0, nil)
}

func configMemcacheKey() string {
	return "appconfig"
}
