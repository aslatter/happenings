package main

import "html/template"
import "net/http"

import "google.golang.org/appengine"

// We wrap the generic `HandleFunc` with one that redirects to https
// (except for on the dev server).
func handleFunc(path string, handler func(http.ResponseWriter, *http.Request)) {
	http.HandleFunc(path, func(w http.ResponseWriter, req *http.Request) {

		httpAllowed := appengine.IsDevAppServer() ||
			req.Header.Get("X-AppEngine-Cron") == "true"

		if req.URL.Scheme == "http" && !httpAllowed {
			newUrl := *req.URL
			newUrl.Scheme = "https"
			http.Redirect(w, req, newUrl.String(), 302)
			return
		}
		handler(w, req)
	})
}

// Html templates for use elsewhere
var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("app/templates/*.html"))
}
