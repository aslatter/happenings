package main

import "net/http"

import "google.golang.org/appengine"
import "google.golang.org/appengine/log"

import "gitlab.com/aslatter/happenings/config"

func init() {
	handleFunc("/admin/config", configHandler)
}

func configHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		configGet(w, r)
	} else if r.Method == "POST" {
		configPost(w, r)
	} else {
		http.Error(w, "Method not supported", 405)
	}
}

func configGet(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	conf, err := config.GetConfig(c)

	if err != nil {
		http.Error(w, "Internal server error", 500)
		log.Errorf(c, "Error getting config: %s", err)
		return
	}

	w.Header().Set("Content-Type", "text/html")
	err = tpl.ExecuteTemplate(w, "config.html", conf)
	if err != nil {
		log.Errorf(c, "Error execute template: %s", err)
	}
}

func configPost(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	// Update config
	err := config.UpdateConfig(c, func(conf *config.Config) (*config.Config, error) {

		// Basic settings
		conf.LegistarBaseURL = r.FormValue("baseUrl")
		conf.TimeLocationName = r.FormValue("timezone")
		conf.UpdatesEnable = r.FormValue("updates") == "on"

		// Twitter
		conf.TwitterEnable = r.FormValue("twitterEnable") == "on"
		conf.TwitterKey = r.FormValue("twitterKey")
		conf.TwitterSecret = r.FormValue("twitterSecret")
		conf.TwitterToken = r.FormValue("twitterToken")
		conf.TwitterTokenSecret = r.FormValue("twitterTokenSecret")

		return conf, nil
	})

	if err != nil {
		log.Errorf(c, "Error saving config: %s", err)
		http.Error(w, "Internal server error", 500)
		return
	}

	configGet(w, r)
}
