package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/delay"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"

	"gitlab.com/aslatter/happenings/config"
	"gitlab.com/aslatter/happenings/legistar"
	"gitlab.com/aslatter/happenings/twitter"
)

func init() {
	handleFunc("/admin/loadCalendar", loadCalendar)
	handleFunc("/admin/testQuery", testQuery)
}

func loadCalendar(w http.ResponseWriter, r *http.Request) {
	// "GET" only
	if r.Method != "GET" {
		http.Error(w, "Method not supported", 405)
		return
	}

	ctx := appengine.NewContext(r)
	conf, err := config.GetConfig(ctx)
	if err != nil {
		log.Errorf(ctx, "Failed loading configuration: %s", err)
		http.Error(w, "", 500)
		return
	}

	timeLoc, _ := time.LoadLocation(conf.TimeLocationName)

	query := new(legistar.CalendarQuery)
	query.BaseUrl = conf.LegistarBaseURL
	query.TimeLocation = timeLoc

	client := urlfetch.Client(ctx)

	result := legistar.LoadCalendarEntries(query, client)

	if result == nil {
		http.Error(w, "Unknown server error", 500)
		log.Errorf(ctx, "Error in LoadCalendar query!")
		return
	}

	err = fileCommitteeData(ctx, result.Committees)
	if err != nil {
		http.Error(w, "Unknown server error", 500)
		log.Errorf(ctx, "Error filing committees: %s", err)
		return
	} else {
		log.Infof(ctx, "Filed committees")
	}

	err = fileMeetings(ctx, timeLoc, result.CalendarEntries)
	if err != nil {
		http.Error(w, "Unknown server error", 500)
		log.Errorf(ctx, "Error filing meetings: %s", err)
		return
	} else {
		log.Infof(ctx, "Filed meetings")
	}
}

// I think I probably want to check for committee changes before filing new committees, but
// multi-get throws if I try to get a committee that doesn't exist.
// I might be able to construct some key-only-query based on key, but that seems werid.
func fileCommitteeData(ctx context.Context, committees map[string]legistar.Committee) error {

	toFile := make([]legistar.Committee, len(committees))
	toFile = toFile[0:0]

	keys := make([]*datastore.Key, len(committees))
	keys = keys[0:0]

	for uuid, committee := range committees {
		key := datastore.NewKey(ctx, "committee", uuid, 0, nil)
		toFile = append(toFile, committee)
		keys = append(keys, key)
	}

	// File!
	if len(toFile) == 0 {
		return nil
	}

	_, err := datastore.PutMulti(ctx, keys, toFile)
	return err
}

// The parent key for all meetings
func meetingsGroupKey(ctx context.Context) *datastore.Key {
	return datastore.NewKey(ctx, "calendar", "entries", 0, nil)
}

func fileMeetings(ctx context.Context, timeLoc *time.Location, inputMeetings map[string]legistar.CalendarEntry) error {
	var oneChange *legistar.CalendarChange
	meetings := make(map[string]legistar.CalendarEntry)
	for k, v := range inputMeetings {
		meetings[k] = v
	}

	// We file meetings into an entity group so we can do transactions
	groupKey := meetingsGroupKey(ctx)

	toFile := make([]legistar.CalendarEntry, len(meetings))
	toFile = toFile[0:0]

	fileKeys := make([]*datastore.Key, len(meetings))
	fileKeys = fileKeys[0:0]

	changes := make([]*legistar.CalendarChange, len(meetings))
	changes = changes[0:0]

	// Update existing meetings
	err, oldMeetings := loadWindowMeetings(ctx, timeLoc)
	if err != nil {
		return err
	}

	log.Infof(ctx, "Loaded %v old meetings", len(oldMeetings))
	log.Infof(ctx, "Have %v scraped meetings", len(meetings))

	for _, oldMeeting := range oldMeetings {
		newMeeting := meetings[oldMeeting.UUID]
		if newMeeting.UUID == "" {
			// we failed the lookup
			log.Infof(ctx, "Old meeting not present in new data %s @ %s [%s]",
				oldMeeting.Committee.Name,
				oldMeeting.LocalTime,
				oldMeeting.UUID)
			continue
			// TODO - remove oldMeeting.UUID from GAE?
		}
		if newMeeting.UUID != oldMeeting.UUID {
			log.Errorf(ctx, "Key error for %s @ %s",
				newMeeting.Committee.Name,
				newMeeting.LocalTime)
			// TODO create an error object somehow
			return nil
		}

		if newMeeting.IsDifferent(&oldMeeting) {
			fileKeys = append(fileKeys, datastore.NewKey(ctx, "calendar", newMeeting.UUID, 0, groupKey))
			toFile = append(toFile, newMeeting)

			oneChange = new(legistar.CalendarChange)
			oneChange.OldEntry = &oldMeeting
			oneChange.NewEntry = &newMeeting
			changes = append(changes, oneChange)

			log.Infof(ctx, "Updating %s @ %s",
				newMeeting.Committee.Name,
				newMeeting.LocalTime.Format("2006-01-02 15:04"),
			)
		}
		delete(meetings, newMeeting.UUID)
	}

	log.Infof(ctx, "Have %v new meetings after trimming old", len(meetings))

	// Create new meetings
	for uuid, newMeeting := range meetings {
		// Validate that the meeting and key match
		if newMeeting.UUID != uuid {
			log.Errorf(ctx,
				"Key error for %s @ %s",
				newMeeting.Committee.Name,
				newMeeting.LocalTime)
		}
		fileKeys = append(fileKeys, datastore.NewKey(ctx, "calendar", uuid, 0, groupKey))
		toFile = append(toFile, newMeeting)

		oneChange = new(legistar.CalendarChange)
		oneChange.NewEntry = &newMeeting
		changes = append(changes, oneChange)

		log.Infof(ctx, "Creating %s @ %s",
			newMeeting.Committee.Name,
			newMeeting.LocalTime.Format("2006-01-02 15:04"),
		)
	}

	if len(toFile) == 0 {
		log.Infof(ctx, "No meeting updates needed")
		return nil
	}

	_, err = datastore.PutMulti(ctx, fileKeys, toFile)
	if err != nil {
		return err
	}

	conf, _ := config.GetConfig(ctx)
	if conf.UpdatesEnable {
		delayProcessChanges.Call(ctx, changes)
	} else {
		log.Infof(ctx, "Updates supressed")
	}

	return nil
}

func testQuery(w http.ResponseWriter, r *http.Request) {

	ctx := appengine.NewContext(r)
	conf, err := config.GetConfig(ctx)
	if err != nil {
		http.Error(w, "Unknown server error", 500)
		return
	}

	// Query all of the meetings which exist in the db for the current date-range
	timeLoc, _ := time.LoadLocation(conf.TimeLocationName)

	// List meetings
	err, meetings := loadWindowMeetings(ctx, timeLoc)
	if err != nil {
		http.Error(w, "Unknown server error", 500)
		return
	}

	fmt.Fprintf(w, "<!DOCTYPE html>")
	fmt.Fprintf(w, "<html>")
	fmt.Fprintf(w, "<body>")
	fmt.Fprintf(w, "<p>Hi!")

	fmt.Fprintf(w, "<p>Found %d results", len(meetings))

	fmt.Fprintf(w, "<ul>")
	for _, mtg := range meetings {
		fmt.Fprintf(w, "<li><ul>")
		fmt.Fprintf(w, "<li>%s", mtg.Committee.Name)
		fmt.Fprintf(w, "<li>%s", mtg.Location)
		fmt.Fprintf(w, "<li>%s", mtg.LocalTime)
		fmt.Fprintf(w, "</ul>")
	}

	fmt.Fprintf(w, "</ul>")

	fmt.Fprintf(w, "</body>")
	fmt.Fprintf(w, "</html>")
}

// Load events already in GAE in our update-window (last month, this month, next month).
func loadWindowMeetings(ctx context.Context, timeLoc *time.Location) (error, []legistar.CalendarEntry) {

	now := time.Now().In(timeLoc)

	// We want to load from GAE all meetings from last month, this month
	// and next month.
	upperYear, upperMonth, _ := now.AddDate(0, 2, 0).Date()
	lowerYear, lowerMonth, _ := now.AddDate(0, -1, 0).Date()

	upperDate := time.Date(upperYear, upperMonth, 1, 0, 0, 0, 0, timeLoc)
	lowerDate := time.Date(lowerYear, lowerMonth, 1, 0, 0, 0, 0, timeLoc)

	// TODO - convert times to UTC for query?
	//  Or does GAE do that for us? It might be better
	//  to be explicit about this.
	groupKey := meetingsGroupKey(ctx)
	q := datastore.NewQuery("calendar").
		Filter("Time <", upperDate).
		Filter("Time >=", lowerDate).
		Ancestor(groupKey)

	var meetings []legistar.CalendarEntry
	if _, err := q.GetAll(ctx, &meetings); err != nil {
		return err, nil
	}

	return nil, meetings
}

// Delay-function for handling updates to calendar entries.
// This does all signaling to external systems.
var delayProcessChanges = delay.Func("processChanges",
	func(c context.Context, changes []*legistar.CalendarChange) error {

		conf, err := config.GetConfig(c)
		if err != nil {
			return err
		}

		if !conf.UpdatesEnable {
			return nil
		}

		// Twitter is our only update channel,
		// so no need to continue if we're not
		// Tweeting.
		if !conf.TwitterEnable {
			log.Infof(c, "Not tweeting right now")
			return nil
		}

		var t *twitter.Twitter
		err = nil
		if conf.TwitterEnable {
			t, err = twitter.NewTwitter(c)
		}
		if err != nil {
			log.Errorf(c, "%s", err)
			return err
		}

		log.Infof(c, "Processing changes")

		for _, oneChange := range changes {
			processOneChange(c, t, oneChange)
		}

		return nil
	})

func processOneChange(c context.Context, t *twitter.Twitter, change *legistar.CalendarChange) {
	// For now, we only handle cases that have old and new data
	if change.NewEntry == nil {
		return
	}
	if change.OldEntry == nil {
		return
	}

	// Added minutes
	if change.OldEntry.Minutes == "" && change.NewEntry.Minutes != "" {
		updatedMinutes(c, t, change.NewEntry)
	}

	// Added agenda
	if change.OldEntry.Agenda == "" &&
		change.NewEntry.Agenda != "" &&
		change.NewEntry.Time.After(time.Now()) {
		updatedAgenda(c, t, change.NewEntry)
	}
}

func updatedMinutes(c context.Context, t *twitter.Twitter, entry *legistar.CalendarEntry) {
	prefix := trimMessage(fmt.Sprintf("Minutes posted for %s", entry.ShortString()), 116)
	message := fmt.Sprintf("%s %s",
		prefix,
		entry.Minutes)
	subject := fmt.Sprintf("Minutes for %s", entry.Committee.Name)

	update(c, t, message, subject)
}

func updatedAgenda(c context.Context, t *twitter.Twitter, entry *legistar.CalendarEntry) {
	prefix := trimMessage(fmt.Sprintf("Agenda posted for %s", entry.ShortString()), 116)
	message := fmt.Sprintf("%s %s",
		prefix,
		entry.Agenda)
	subject := fmt.Sprintf("Agenda for %s", entry.Committee.Name)

	update(c, t, message, subject)
}

func trimMessage(message string, length int) string {
	if len(message) > length {
		return message[0:length-4] + "..."
	} else {
		return message
	}
}

func update(c context.Context, t *twitter.Twitter, message string, subject string) {
	log.Infof(c, "Update: %s", message)

	err := t.Tweet(message)

	if err != nil {
		log.Errorf(c, "Error sending tweet \"%s\": %s",
			message,
			err)
	}
}
