package main

import "net/http"

import "golang.org/x/net/context"

import "google.golang.org/appengine"
import "google.golang.org/appengine/log"

import "gitlab.com/aslatter/happenings/twitter"

func init() {
	handleFunc("/admin/status", statusHandler)
}

func statusHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		statusGet(w, r)
	} else {
		http.Error(w, "Method not supported", 405)
	}
}

func statusGet(w http.ResponseWriter, r *http.Request) {

	c := appengine.NewContext(r)
	status := make(map[string]string)

	status["Twitter"] = checkTwitterStatus(c)

	err := tpl.ExecuteTemplate(w, "status.html", status)

	if err != nil {
		log.Errorf(c, "Error execute template: %s", err)
		http.Error(w, "Unknown server error", 500)
	}
}

func checkTwitterStatus(c context.Context) string {
	t, err := twitter.NewTwitter(c)
	if err != nil {
		return err.Error()
	}

	return t.Status()
}
