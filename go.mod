module gitlab.com/aslatter/happenings

go 1.11

require (
	github.com/PuerkitoBio/goquery v0.3.3-0.20160526121220-aeecb06b1e04
	github.com/andybalholm/cascadia v0.0.0-20150730174459-3ad29d1ad1c4 // indirect
	github.com/kurrik/oauth1a v0.0.0-20151019171716-cb1b80e32dd4
	github.com/kurrik/twittergo v0.0.0-20160609170749-1152e2e0077d
	golang.org/x/net v0.0.0-20190603091049-60506f45cf65
	google.golang.org/appengine v1.6.5
)
