# Project

## Goals

Design a bot to scrape a Legistar calendar and notify interested parties of
changes. For example, if a new agenda is posted we could tweet about it. Or we
could summarize daily changes into a blog.

## Technical requirements

Stay within the free-tier of a cloud-service provider.

## Current status

This is implemented on Google's App Engine in the **go** language.

# Admin pages

* `/admin/config` - Modify app settings
* `/admin/loadCalendar` - Scrape calendar data and trigger updates
* `/admin/testQuery` - Print calendar entries for the current month

# Processes

This assumes you have a functional go workspace set up.

## Deploy to dev

Push to trunk.

## Deploy to prod

Push a tag to trunk.

## Adding new dependencies

    go get $DEPENDENCY
    # update & test application
    godep save ./...
    git add vendor/*** Godeps/***
    git commit -m "Add $DEPENDENCY"
    # add & commit application changes

## Updating dependencies

    godep restore # ??? https://github.com/tools/godep/issues/453
    go get -u all
    godep update ...
    # test applicaiton
    git add vendor/*** Godeps/***
    git commit -m "Updating dependencies."

    