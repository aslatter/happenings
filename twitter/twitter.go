package twitter

import "fmt"
import "net/http"
import "net/url"
import "strings"

import "golang.org/x/net/context"

import "gitlab.com/aslatter/happenings/config"

import "github.com/kurrik/oauth1a"
import "github.com/kurrik/twittergo"

import "google.golang.org/appengine/urlfetch"

type Twitter struct {
	TwitterGo *twittergo.Client
}

func NewTwitter(c context.Context) (*Twitter, error) {
	config, err := config.GetConfig(c)
	if err != nil {
		return nil, err
	}

	clientConfig := &oauth1a.ClientConfig{
		ConsumerSecret: config.TwitterSecret,
		ConsumerKey:    config.TwitterKey}
	userConfig := oauth1a.NewAuthorizedConfig(
		config.TwitterToken,
		config.TwitterTokenSecret)

	twitterClient := twittergo.NewClient(clientConfig, userConfig)
	twitterClient.HttpClient = urlfetch.Client(c)

	return &Twitter{TwitterGo: twitterClient}, nil
}

// Returns a human-readble string on the current
// availablility of this service.
func (t *Twitter) Status() string {
	req, err := http.NewRequest("GET", "/1.1/account/verify_credentials.json", nil)
	if err != nil {
		return err.Error()
	}

	resp, err := t.TwitterGo.SendRequest(req)
	if err != nil {
		return err.Error()
	}

	user := &twittergo.User{}
	err = resp.Parse(user)
	if err != nil {
		return err.Error()
	}

	return fmt.Sprintf("Twitter okay! Currently logged in as %v.", user.Name())
}

func (t *Twitter) Tweet(message string) error {
	tweet := url.Values{}

	// See https://dev.twitter.com/rest/reference/post/statuses/update
	tweet.Set("status", message)
	tweet.Set("trim_user", "true")

	body := strings.NewReader(tweet.Encode())
	req, err := http.NewRequest("POST", "/1.1/statuses/update.json", body)
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := t.TwitterGo.SendRequest(req)
	if err != nil {
		return err
	}

	// If we don't try and parse the response
	// we won't see all error messages.
	newTweet := &twittergo.Tweet{}
	err = resp.Parse(newTweet)
	return err
}
