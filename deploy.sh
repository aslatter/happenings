#!/bin/bash

set -eu
set -o pipefail

# Download SDK

SDK_URL="https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-280.0.0-linux-x86_64.tar.gz"

SDK_FOLDER=$(mktemp -d)
function cleanup() {
  rm -rf "$SDK_FOLDER"
}
trap cleanup EXIT

GCLOUD="$SDK_FOLDER/google-cloud-sdk/bin/gcloud"
curl "$SDK_URL" | gunzip | tar -x -C "$SDK_FOLDER"

# install auth

echo "$PRIVATE_SERVICE_AUTH" > service.json

$GCLOUD auth activate-service-account "$PRIVATE_SERVICE_NAME" --key-file service.json

# Deploy!

$GCLOUD app deploy app/app.yaml app/cron.yaml --version=v1 --project=$APPLICATION --quiet
