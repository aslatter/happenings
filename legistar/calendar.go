package legistar

import "fmt"
import "log"
import "net/http"
import "net/url"
import "strings"
import "time"

import "github.com/PuerkitoBio/goquery"

import "google.golang.org/appengine/datastore"

type CalendarQuery struct {
	BaseUrl      string
	TimeLocation *time.Location
}

type CalendarQueryResult struct {
	Committees      map[string]Committee
	CalendarEntries map[string]CalendarEntry
}

type CalendarEntry struct {
	UUID      string
	Committee Committee
	Time      time.Time // absolute time of meeting
	LocalTime time.Time // date+time of event (for display only - other fields are zeroed)
	Location  string
	Agenda    string // optional
	Minutes   string // optional
}

func (oldMeeting *CalendarEntry) IsDifferent(newMeeting *CalendarEntry) bool {
	if oldMeeting.Location != newMeeting.Location {
		return true
	}
	if oldMeeting.Agenda != newMeeting.Agenda {
		return true
	}
	if oldMeeting.Minutes != newMeeting.Minutes {
		return true
	}
	if oldMeeting.Committee != newMeeting.Committee {
		return true
	}
	if !oldMeeting.Time.Equal(newMeeting.Time) {
		return true
	}
	if !oldMeeting.LocalTime.Equal(newMeeting.LocalTime) {
		return true
	}

	return false
}

func (entry *CalendarEntry) ShortString() string {
	return fmt.Sprintf("%s meeting of %s",
		entry.LocalTime.Format("1/2"),
		entry.Committee.Name)
}

type Committee struct {
	UUID string
	Name string
	URL  string
}

type CalendarChange struct {
	OldEntry *CalendarEntry
	NewEntry *CalendarEntry
}

type changeOldOnly struct {
	OldEntry CalendarEntry
}

type changeNewOnly struct {
	NewEntry CalendarEntry
}

type changeBoth struct {
	OldEntry CalendarEntry
	NewEntry CalendarEntry
}

func (change *CalendarChange) Load(p []datastore.Property) error {
	var changes changeBoth
	err := datastore.LoadStruct(&changes, p)

	if changes.OldEntry.UUID != "" {
		change.OldEntry = &changes.OldEntry
	}
	if changes.NewEntry.UUID != "" {
		change.NewEntry = &changes.NewEntry
	}

	return err
}

func (change *CalendarChange) Save() ([]datastore.Property, error) {
	if change.OldEntry == nil {
		if change.NewEntry == nil {
			return nil, fmt.Errorf("calendar: missing both old and new from change")
		}

		return datastore.SaveStruct(
			changeNewOnly{NewEntry: *change.NewEntry})
	}

	if change.NewEntry == nil {
		if change.OldEntry == nil {
			return nil, fmt.Errorf("calendar: missing both old and new from change")
		}

		return datastore.SaveStruct(
			changeOldOnly{OldEntry: *change.OldEntry})
	}

	return datastore.SaveStruct(
		changeBoth{OldEntry: *change.OldEntry, NewEntry: *change.NewEntry})
}

func LoadCalendarEntries(params *CalendarQuery, client *http.Client) *CalendarQueryResult {
	baseUrl := params.BaseUrl
	timeLoc := params.TimeLocation

	result := new(CalendarQueryResult)
	committees := make(map[string]Committee)
	entries := make(map[string]CalendarEntry)

	if client == nil {
		client = &http.Client{}
	}
	client.Jar = nil

	months := []string{
		lastMonth,
		thisMonth,
		nextMonth,
	}

	for _, month := range months {

		log.Printf("LoadCalendar: About to request %s", month)

		reqUrl, err := url.Parse(baseUrl)
		if err != nil {
			log.Printf("Error parsing URL %s: %s", baseUrl, err)
			return nil
		}

		reqUrl.Path = "Calendar.aspx"
		reqQuery := reqUrl.Query()
		reqQuery.Add("Mode", month)
		reqUrl.RawQuery = reqQuery.Encode()

		log.Printf("Requesting %s", reqUrl.String())

		req, _ := http.NewRequest("GET", reqUrl.String(), nil)

		resp, err := client.Do(req)
		if err != nil {
			log.Printf("Load calendar error: %s", err)
			return nil
		}
		defer resp.Body.Close()

		document, err := goquery.NewDocumentFromResponse(resp)
		if err != nil {
			log.Printf("Load calendar error: %s", err)
			return nil
		}

		document.Selection.Find("table.rgMasterTable tr").Each(func(i int, s *goquery.Selection) {
			// Skip the header row
			if i == 0 {
				return
			}

			meeting, committee := buildEntryFromRow(s, timeLoc, baseUrl)
			if meeting != nil && committee != nil {
				entries[meeting.UUID] = *meeting
				committees[committee.UUID] = *committee
			}
		})
	}

	result.CalendarEntries = entries
	result.Committees = committees

	return result
}

func buildEntryFromRow(s *goquery.Selection, timeLoc *time.Location, baseUrl string) (*CalendarEntry, *Committee) {
	var meeting *CalendarEntry
	var committee *Committee

	columns := s.Find("td")

	// Every meeting has an iCal link. We parse the GUID for
	// the meeting out of that.
	iCalLink := columns.Find("a").Slice(1, 2)
	meetingUuid := guuidFromLink(iCalLink)

	// Time & date of the meeting are in cells 2 and 4
	dateString := strings.TrimSpace(columns.Slice(1, 2).Text())
	timeString := strings.TrimSpace(columns.Slice(3, 4).Text())

	// TODO - add 'Canceled' field to calendar entry
	if timeString == "Canceled" {
		return nil, nil
	}

	mtgTime, _ := time.ParseInLocation("1/2/2006 3:04 PM",
		dateString+" "+timeString,
		timeLoc)

	// Location plus other text is in column 5
	location := strings.TrimSpace(columns.Slice(4, 5).Text())

	// Agenda and minutes may not be posted, but if they are
	// we want a link to them
	agendaLink := hrefFromLink(columns.Slice(5, 6).Find("a"))
	minutesLink := hrefFromLink(columns.Slice(6, 7).Find("a"))
	if agendaLink != "" {
		agendaLink = baseUrl + agendaLink
	}
	if minutesLink != "" {
		minutesLink = baseUrl + minutesLink
	}

	// Build committee from first column link
	committee = parseCommittee(s, baseUrl)

	if committee != nil {
		// 'time' has been parsed into a time+offset.
		// once we store it in GAE it will be normalized to UTC.
		// So we build the struct with times already in UTC.
		// The 'Time' field is the absolute time, but the 'LocaTime'
		// field claims to be UTC but that is a lie - it is the date + time
		// in wall-clock that we can use for display.
		utcLocation, _ := time.LoadLocation("UTC")
		localTime := time.Date(mtgTime.Year(),
			mtgTime.Month(),
			mtgTime.Day(),
			mtgTime.Hour(),
			mtgTime.Minute(),
			0, 0, utcLocation)

		meeting = new(CalendarEntry)
		meeting.Committee = *committee
		meeting.UUID = meetingUuid
		meeting.Time = mtgTime.UTC()
		meeting.LocalTime = localTime
		meeting.Location = location
		meeting.Agenda = agendaLink
		meeting.Minutes = minutesLink
	}

	return meeting, committee
}

// Given a link, extract the "GUID" query parameter
func guuidFromLink(link *goquery.Selection) string {
	href := hrefFromLink(link)
	var uuid string
	if href != "" {
		url, _ := url.Parse(href)
		if url != nil {
			uuid = url.Query()["GUID"][0]
		}
	}
	return uuid
}

// Given an anchor element, extract the "href" attribute
func hrefFromLink(link *goquery.Selection) string {
	if link.Length() == 0 {
		return ""
	}
	node := link.Get(0)
	var href string
	for _, att := range node.Attr {
		if att.Key == "href" {
			href = att.Val
			break
		}
	}
	return href
}

// Given a row in the calendar table, parse the committee
// out of the HTML into a discrete `Committee` struct.
func parseCommittee(row *goquery.Selection, baseUrl string) *Committee {

	var mtgCommittee *Committee = nil

	committee := row.Find("a").First()
	if committee.Length() >= 1 {
		href := hrefFromLink(committee)
		if href != "" {
			mtgCommittee = new(Committee)
			var foundURL *url.URL
			foundURL, _ = url.Parse(href)
			mtgCommittee.URL = baseUrl + href
			params := foundURL.Query()
			mtgCommittee.UUID = params["GUID"][0]
			mtgCommittee.Name = strings.TrimSpace(committee.Text())
		}
	}

	return mtgCommittee
}

// Calendar options:
//   "Last Month"
//   "This Month"
//   "Next Month"
// Can be placed in to cookie:
//    "Setting-205-Calendar Year"

const (
	lastMonth string = "Last Month"
	thisMonth        = "This Month"
	nextMonth        = "Next Month"
)
